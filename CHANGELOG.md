# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.1.1"></a>
## [0.1.1](https://bitbucket.org/kokengthecoder/ufinity_test_assessment/compare/v0.1.0...v0.1.1) (2018-10-19)


### Bug Fixes

* **stacktrace:** remove stack property if not in development ([ed1dfc0](https://bitbucket.org/kokengthecoder/ufinity_test_assessment/commits/ed1dfc0))



<a name="0.1.0"></a>
# 0.1.0 (2018-10-17)


### Features

* **commonstudent:** feature implemented and tested ([696b3f2](https://bitbucket.org/kokengthecoder/ufinity_test_assessment/commits/696b3f2))
* **register:** basic function works ([c984b0a](https://bitbucket.org/kokengthecoder/ufinity_test_assessment/commits/c984b0a))
* **retrievefornotifications:** feature implemented test passed ([45693ea](https://bitbucket.org/kokengthecoder/ufinity_test_assessment/commits/45693ea))
* **suspend:** suspend function added, tested done ([c73788a](https://bitbucket.org/kokengthecoder/ufinity_test_assessment/commits/c73788a))
