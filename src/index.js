import config from './config'
import app from './init/express'
import db from './init/sequelize'

const { appName, port } = config

db.sequelize.sync((err) => {
  if (err) throw err
  console.log('Database connected')
})

app.listen(port, () => {
  console.log(`${appName} listening at port: ${port}`)
})

export default app
