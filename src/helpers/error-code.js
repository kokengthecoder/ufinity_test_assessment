import APIError from './api-error'
import httpStatus from 'http-status'

export default {
  studentNotFound: new APIError('Student not found', httpStatus.NOT_FOUND, true)
}
