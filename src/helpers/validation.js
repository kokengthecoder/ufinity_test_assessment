import Joi from 'joi'

export default {
  register: {
    body: {
      teacher: Joi.string().email().required(),
      students: Joi.array().items(Joi.string().email()).min(1)
    }
  },

  commonStudents: {
    query: {
      teacher: Joi.alternatives().try(
        Joi.string().email(),
        Joi.array().items(Joi.string().email())
      )
    }
  },

  retrieveForNotifications: {
    body: {
      teacher: Joi.string().email().required(),
      notification: Joi.string().required()
    }
  },

  suspend: {
    body: {
      student: Joi.string().email().required()
    }
  }
}
