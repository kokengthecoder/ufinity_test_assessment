import express from 'express'
import validate from 'express-validation'

// Controllers
import registerController from '../controllers/register.controller'
import commonStudentsController from '../controllers/common-students.controller'
import suspendController from '../controllers/suspend.controller'
import retrieveForNotificationsController from '../controllers/retrieve-for-notifications.controller.js'

// Validation Schema
import validationSchema from '../helpers/validation'

const router = express.Router()

router.use('/health-check', (req, res) =>
  res.send('OK')
)

router.post(
  '/register',
  validate(validationSchema.register),
  registerController
)

router.get(
  '/commonstudents',
  validate(validationSchema.commonStudents),
  commonStudentsController
)

router.post(
  '/suspend',
  validate(validationSchema.suspend),
  suspendController
)

router.post(
  '/retrievefornotifications',
  validate(validationSchema.retrieveForNotifications),
  retrieveForNotificationsController
)

export default router
