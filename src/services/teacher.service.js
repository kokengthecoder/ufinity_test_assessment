import db from '../init/sequelize'

const debug = require('debug')('service:teacher')

/**
 * Register teacher if not found
 * Register students if not found
 * Register lessons based on teachers and students
 * @param {string} teacherEmail - teacher email
 * @param {[string]} studentEmails - list of students' email
 * @returns {Promise<Model[]>}
 */
const registerStudentAndLesson = (teacherEmail, studentEmails) => {
  return new Promise(async (resolve, reject) => {
    try {
      /**
       * Create a new teacher if not found
       * use spread to only return teacher
       * teacher: Model
       */
      const teacher = await db.teacher
        .findOrCreate({
          where: { email: teacherEmail }
        })
        .spread((teacher, created) => teacher)

      /**
       * Check if any students already registered under teacher
       * studentInLessons: Model[]
       */
      const studentInLessons = await teacher.getStudents({
        where: { email: studentEmails }
      })

      debug('studentInLessons', studentInLessons)

      /**
       * Filter pending registration lessons
       * pendingStudentEmails: email[]
       */
      const pendingStudentEmails =
        studentInLessons.length < 1
          ? studentEmails
          : studentEmails
            .filter(email =>
              (studentInLessons.map(st => st.email))
                .indexOf(email) === -1
            )

      debug('pendingStudentEmails', pendingStudentEmails)

      /**
       * Return if all student already registered
       */
      if (pendingStudentEmails.length < 1) {
        return resolve([])
      }

      /**
       * Find registered student in student table
       * registeredStudent: Model[]
       */
      const registeredStudent = await db.student
        .findAll({
          where: { email: pendingStudentEmails }
        })

      debug('registered student', registeredStudent)

      /**
       * filter unregistered student by filter out duplicate value
       * from pendingStudentEmails and registered student emails
       * unregisteredStudentEmails: email[]
       */
      const unregisteredStudentEmails =
       registeredStudent.length < 1
         ? pendingStudentEmails
         : registeredStudent
           .filter(st => pendingStudentEmails.indexOf(st.email) === -1)
           .map(st => st.email)

      debug('unregistered student', unregisteredStudentEmails)

      /**
       * register new students
       * transform list emails into list of objects
       * new students: [{ email: 'student@gmail.com' }, ...]
       */
      const newStudents = await db.student
        .bulkCreate(
          unregisteredStudentEmails.map(email => { return { email } }),
          { validate: true }
        )

      debug('new students', newStudents)

      /**
       * create lessons
       * use spread to return first element of array
       * lessons: Model[]
       */
      const lessons = await teacher
        .addStudents([
          ...registeredStudent,
          ...newStudents
        ])
        .spread(lessons => lessons)

      debug('lessons', lessons)

      resolve(lessons)
    } catch (err) {
      debug(err)
      reject(err)
    }
  })
}

export default {
  registerStudentAndLesson
}
