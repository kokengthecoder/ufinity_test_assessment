import Joi from 'joi'
import 'dotenv/config'

const debug = require('debug')('app:config')

const envSchema = Joi.object().keys({
  // General config
  NODE_ENV: Joi.string()
    .allow(['development', 'production', 'test', 'provision'])
    .default('development'),
  APP_NAME: Joi.string().default('app'),
  PORT: Joi.string().default('8080'),

  // Database config
  MYSQL_HOST: Joi.string(),
  MYSQL_USER: Joi.string(),
  MYSQL_PASSWORD: Joi.string(),
  MYSQL_DATABASE: Joi.string(),
  MYSQL_LOGGING: Joi.boolean()
    .when('NODE_ENV', {
      is: Joi.string().equal('development'),
      then: Joi.boolean().default(true),
      otherwise: Joi.boolean().default(false)
    }),

  // AWS RDS Database config
  RDS_HOSTNAME: Joi.string(),
  RDS_USERNAME: Joi.string(),
  RDS_PASSWORD: Joi.string(),
  RDS_DB_NAME: Joi.string()
}).unknown()

const { error, value: env } = Joi.validate(process.env, envSchema)
if (error) throw error

const config = {
  nodeEnv: env.NODE_ENV,
  appName: env.APP_NAME,
  port: env.PORT,
  dbConfig: {
    host: env.MYSQL_HOST || env.RDS_HOSTNAME,
    user: env.MYSQL_USER || env.RDS_USERNAME,
    password: env.MYSQL_PASSWORD || env.RDS_PASSWORD,
    database: env.NODE_ENV === 'production'
      ? env.MYSQL_DATABASE || env.RDS_DB_NAME 
      : `${env.MYSQL_DATABASE || env.RDS_DB_NAME}_${env.NODE_ENV}`,
    logging: env.MYSQL_LOGGING
  }
}

debug(config)

export default config
