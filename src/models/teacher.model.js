module.exports = (sequelize, Sequelize) => {
  const Teacher = sequelize.define('teacher', {
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        isEmail: true
      }
    }
  }, {
    indexes: [
      {
        unique: true,
        fields: ['email']
      }
    ]
  })

  return Teacher
}
