module.exports = (sequelize, Sequelize) => {
  const Student = sequelize.define('student', {
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        isEmail: true
      }
    }
  }, {
    indexes: [
      {
        unique: true,
        fields: ['email']
      }
    ]
  })

  return Student
}
