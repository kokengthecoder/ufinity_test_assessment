module.exports = (sequelize, Sequelize) => {
  const Lesson = sequelize.define('lesson', {
    suspend: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  })

  return Lesson
}
