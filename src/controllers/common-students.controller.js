import db from '../init/sequelize'

const { sequelize, Sequelize } = db

const debug = require('debug')('controller:common-students')

async function getCommonStudents (req, res, next) {
  try {
    let { teacher: teachers } = req.query

    // change is to true to search suspended student
    let suspend = false

    // convert string to array if not array
    const teacherEmails = Array.isArray(teachers) ? teachers : [teachers]
    debug(teacherEmails)

    const students = await db.student.findAll({
      includeIgnoreAttributes: false,
      attributes: [
        'email',
        [sequelize.fn('count', sequelize.col('teachers.id')), 'teachers_count']
      ],
      include: [{
        attributes: [],
        model: db.teacher,
        where: {
          email: teacherEmails
        },
        through: {
          attributes: [],
          where: {
            suspend
          }
        }
      }],
      group: ['student.id'],
      having: Sequelize.literal(`count(teachers_count) = ${teacherEmails.length}`)
    })

    debug(students)

    res.status(200).json({
      students: students.map(r => r.email)
    })
  } catch (err) {
    debug(err)
    return next(err)
  }
}

export default getCommonStudents
