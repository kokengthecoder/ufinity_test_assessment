import db from '../init/sequelize'
import errorCode from '../helpers/error-code'

const debug = require('debug')('controller:suspend')

debug('errorCode', errorCode)

const suspend = async (req, res, next) => {
  try {
    const { student: studentEmail } = req.body

    const student = await db.student.findOne({
      where: {
        email: studentEmail
      }
    })

    debug(student)

    // return error if no student found by email provided
    if (!student) throw errorCode.studentNotFound

    const update = await db.lesson.update({
      suspend: true
    }, {
      where: {
        studentId: student.id
      }
    })

    debug(update)

    res.status(204).send()
  } catch (err) {
    debug(err)
    return next(err)
  }
}

export default suspend
