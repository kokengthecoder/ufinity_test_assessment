import db from '../init/sequelize'

const debug = require('debug')('controller:retrieve-for-notifications')

async function retrieveForNotifications (req, res, next) {
  try {
    const {
      teacher: teacherEmail,
      notification
    } = req.body

    // regex to extract email address
    let mentioned = notification.match(/\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+/g)

    // check if mentioned is null
    mentioned = mentioned || []

    // get all non suspend student based on teacher email
    const students = await db.student.findAll({
      includeIgnoreAttributes: false,
      attributes: ['email'],
      include: [{
        attributes: ['id'],
        model: db.teacher,
        where: {
          email: teacherEmail
        },
        through: {
          attributes: [],
          where: {
            suspend: false
          }
        },
        require: false
      }]
    })

    debug(students)

    res.status(200).json({
      // Set stored unique values
      recipients: [ ...new Set([...students.map(std => std.email), ...mentioned]) ]
    })
  } catch (err) {
    debug(err)
    return next(err)
  }
}

export default retrieveForNotifications
