import TeacherService from '../services/teacher.service'

const debug = require('debug')('controller:register')

const register = async (req, res, next) => {
  try {
    const { teacher, students } = req.body

    await TeacherService.registerStudentAndLesson(teacher, students)

    res.status(204).send()
  } catch (err) {
    debug(err)
    return next(err)
  }
}

export default register
