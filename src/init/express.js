import express from 'express'
import expressValidation from 'express-validation'
import APIError from '../helpers/api-error'
import httpStatus from 'http-status'
import config from '../config'

// Routes
import mainRoutes from '../routes'

const app = express()

app.use(express.json())

// enable CORS - Cross Origin Resource Sharing
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

// Get all the routes
app.use('/api', mainRoutes)

// check if the error is Joi validation error
app.use((err, req, res, next) => {
  if (err instanceof expressValidation.ValidationError) {
    // validation error contains errors which is an array of error each containing message[]
    const unifiedErrorMessage = err.errors.map(error => error.messages.join('. ')).join(' and ')
    const error = new APIError(unifiedErrorMessage, err.status, true)

    return next(error)
  } else if (!(err instanceof APIError)) {
    const apiError = new APIError(err.message, err.status, err.isPublic)

    return next(apiError)
  }
  return next(err)
})

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new APIError('API not found', httpStatus.NOT_FOUND)

  return next(err)
})

// error handler, send stacktrace only during development
app.use((err, req, res, next) =>
  res.status(err.status).json({
    message: err.isPublic ? err.message : httpStatus[err.status],
    ...(config.nodeEnv === 'development' ? { stack: err.stack } : {})
  })
)

export default app
