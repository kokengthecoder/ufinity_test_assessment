import Sequelize from 'sequelize'
import config from '../config'

const { dbConfig } = config

const sequelize = new Sequelize(
  dbConfig.database,
  dbConfig.user,
  dbConfig.password,
  {
    host: dbConfig.host,
    dialect: 'mysql',
    operatorsAliases: false,
    logging: dbConfig.logging
  },
)

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.teacher = require('../models/teacher.model')(sequelize, Sequelize)
db.student = require('../models/student.model')(sequelize, Sequelize)
db.lesson = require('../models/lesson.model')(sequelize, Sequelize)

// Setup many to many association
db.teacher.belongsToMany(db.student, { through: db.lesson })
db.student.belongsToMany(db.teacher, { through: db.lesson })

export default db
