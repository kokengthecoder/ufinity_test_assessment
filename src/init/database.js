import mysql from 'mysql2'
import config from '../config'

const { dbConfig } = config
const { database } = dbConfig

const debug = require('debug')('init:database')

/*
  ========================
  Create Mysql Connections
  ========================
*/
const initConnection = () => {
  return mysql.createConnection({
    host: dbConfig.host,
    user: dbConfig.user,
    password: dbConfig.password
    // database not specified here because
    // it will throw error database not found
    // that's why we need to initial a connection without database
    // instead of initial from init/mysql
  })
}

/*
  ======================
  End Connection
  ======================
*/
const endConnection = (db) => {
  return new Promise((resolve, reject) => {
    // Close mysql connection
    db.end((err) => {
      if (err) return reject(err)

      return resolve('MySql connection closed')
    })
  })
}

/*
  ====================
  Create Database
  ====================
*/
const createDatabase = () => {
  return new Promise((resolve, reject) => {
    const db = initConnection()

    let sql = `
      create database if not exists ${database}
    `

    db.query(sql, async (err, result) => {
      if (err) return reject(err)

      debug('created database')

      await endConnection(db)

      return resolve(result)
    })
  })
}

/*
  ====================
  Drop Database
  ====================
*/
const dropDatabase = (db) => {
  return new Promise((resolve, reject) => {
    const db = initConnection()

    let sql = `
      drop database if exists ${database}
    `

    db.query(sql, async (err, result) => {
      if (err) return reject(err)

      debug('drop database')

      await endConnection(db)

      return resolve(result)
    })
  })
}

export default {
  createDatabase,
  dropDatabase
}
