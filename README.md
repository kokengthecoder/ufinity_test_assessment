## Public API Endpoint ##
```
http://ufinity.a2uxdnui3y.ap-southeast-1.elasticbeanstalk.com/
```

## Development / Running Locally ##

####Step 1: Clone the repo####
```
$ git clone https://bitbucket.org/kokengthecoder/ufinity_test_assessment.git
$ cd ufinity_test_assessment
$ yarn
```

####Step 2: Setup Mysql Server####
If you already have mysql server setup locally, skip this step. Recommended using [mysql docker](https://hub.docker.com/_/mysql/)
```
$ docker pull mysql:5.7
$ docker run --name mysql-dev -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=yourpassword mysql:5.7
```

####Step 3: Include ENV file####
Create a **.env** file at root project directory, check src/config for more
```
MYSQL_HOST=localhost
MYSQL_USER=yourusername
MYSQL_PASSWORD=yourpassword
MYSQL_DATABASE=yourdatabasename
```

####Step 4: Create Database####
Create database and table
```
$ yarn create-db
```
A database based on specifed in env {MYSQL_DATABASE}-development will be created

####Step 5: Start node server####
```
$ yarn start-dev
```
Default port will be 8080, add PORT in .env to change different port or specified in command line
```
$ PORT=8081 yarn start-dev
```

## Test ##
To run test, please ensure you have mysql server setup and .env, if not please follow Step 2 and Step 3 in Development / Running Locally
```
$ yarn test
$ yarn test-watch
$ yarn test-coverage
```

## Debug ##
To enable debug mode in register controller, run as following
```
$ DEBUG=controller:register yarn start-dev
```

## Deploy ##
These steps are to deploy to AWS Elastic Beanstalk

#### Step 1: Install EB CLI ####
[AWS Elastic Beanstalk CLI Installation Guide](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-install.html)
```
$ pip install awsebcli --upgrade --user
$ eb --version
```

#### Step 2: Build ####
```
$ yarn build
```

#### Step 3: Create Elastic Beanstalk Environment ####
```
$ eb init
$ eb create -d ufinity -db.engine mysql -db.i db.t2.micro -db.user yourusername -db.pass yourpassword
```

### Trello ###
I organize my task via Trello : [https://trello.com/b/S5HFqrhn/ufinity](https://trello.com/b/S5HFqrhn/ufinity)
