import _ from 'lodash'
import db from '../../src/init/sequelize'
import database from '../../src/init/database'

const debug = require('debug')('test:mysqlHelper')

const before = () => database.createDatabase()

const beforeEach = () => db.sequelize.sync({ force: true })

const afterEach = () => db.sequelize.drop()

const after = () => database.dropDatabase()

const seeding = (dataSets) => {
  return new Promise(async (resolve, reject) => {
    try {
      let teachersEmails = _.map(dataSets, 'teacher')
      let commonStudentsWithEmails = _.intersectionBy(
        ...(_.map(dataSets, 'students'))
      )

      // Register teachers
      const teachers = await db.teacher.bulkCreate(
        _.map(teachersEmails, e => { return { email: e } })
      )

      // Register common students
      const commonStudents = await db.student.bulkCreate(
        _.map(commonStudentsWithEmails, e => { return { email: e } })
      )
      debug('commonStudents', commonStudents)

      // register unique students and lessons and return new students
      const promise = _.map(dataSets, ds => {
        return new Promise(async (resolve, reject) => {
          try {
            // return if teacher no students
            if (!ds.students) resolve([])

            // get teacher instance
            const teacher = teachers.find(t =>
              t.email === ds.teacher
            )

            // get unique student email
            const unregisteredStuEmail = _.xor(
              ds.students,
              commonStudentsWithEmails)

            debug('unregisteredStuEmail', unregisteredStuEmail)

            // create the unique students
            const newStudents = await db.student.bulkCreate(
              _.map(unregisteredStuEmail, e => { return { email: e } })
            )

            // assign student to techer lessons
            const lessons = await teacher.addStudents([
              ...commonStudents,
              ...newStudents
            ]).spread(lessons => lessons)
            debug(lessons)

            return resolve(newStudents)
          } catch (err) {
            return reject(err)
          }
        })
      })

      debug(promise)

      const result = await Promise.all(promise)

      debug(_.flatten(result))
      resolve({
        teachers,
        students: [...commonStudents, ...(_.flatten(result))]
      })
    } catch (err) {
      debug(err)
      reject(err)
    }
  })
}

export default {
  before,
  after,
  beforeEach,
  afterEach,
  seeding
}
