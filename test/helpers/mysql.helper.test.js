import mysqlHelper from './mysql.helper'
import chai from 'chai'

const expect = chai.expect

describe('Mysql Helper: Seeding', () => {
  before(() => mysqlHelper.before())
  beforeEach(() => mysqlHelper.beforeEach())

  it('should create teacher, students and lessons', async () => {
    const dataSets = [
      {
        teacher: 'teacher1@gmail.com',
        students: [
          'student1@gmail.com',
          'student2@gmail.com',
          'teacher1student@gmail.com'
        ]
      },
      {
        teacher: 'teacher2@gmail.com',
        students: [
          'student1@gmail.com',
          'student2@gmail.com',
          'teacher2studet@gmail.com'
        ]
      },
      {
        teacher: 'soloTeacher@gmail.com'
      }
    ]

    const result = await mysqlHelper.seeding(dataSets)

    console.log(result)
    expect(result.teachers).to.have.length(3)
    expect(result.students).to.have.length(4)
  })

  afterEach(() => mysqlHelper.afterEach())
  after(() => mysqlHelper.after())
})
