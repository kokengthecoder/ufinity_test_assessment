import app from '../../src/init/express.js'
import chai from 'chai'
import request from 'supertest'
import mysqlHelper from '../helpers/mysql.helper'
import db from '../../src/init/sequelize'

const expect = chai.expect

const dataSets = [
  {
    teacher: 'teacher1@gmail.com',
    students: [
      'student1@gmail.com'
    ]
  }, {
    teacher: 'teacher2@gmail.com',
    students: [
      'student1@gmail.com'
    ]
  }
]

let students

describe('#POST /api/suspend', () => {
  const url = '/api/suspend'

  before(async () => {
    await mysqlHelper.before()
  })

  beforeEach(async () => {
    await mysqlHelper.beforeEach()
    const seed = await mysqlHelper.seeding(dataSets)
    students = seed.students
  })

  it('should return success suspend student', async () => {
    const res = await request(app)
      .post(url)
      .send({ student: students[0].email })

    expect(res.statusCode).to.equal(204)
    expect(Object.keys(res.body)).to.have.lengthOf(0)

    const result = await db.lesson.findAll({
      where: {
        studentId: students[0].id
      }
    })

    result.map(r => expect(r.suspend).to.be.true)
  })

  it('should return success even if student already suspended', async () => {
    await request(app)
      .post(url)
      .send({ student: students[0].email })

    const res = await request(app)
      .post(url)
      .send({ student: students[0].email })

    expect(res.statusCode).to.equal(204)
    expect(Object.keys(res.body)).to.have.lengthOf(0)

    const result = await db.lesson.findAll({
      where: {
        studentId: students[0].id
      }
    })

    result.map(r => expect(r.suspend).to.be.true)
  })

  it('should return error if student email not valid', async () => {
    const res = await request(app)
      .post(url)
      .send({ student: 'something' })

    expect(res.statusCode).to.equal(400)
    expect(res.body.message).to.have.equal('"student" must be a valid email')
  })

  it('should return error if student not found', async () => {
    const res = await request(app)
      .post(url)
      .send({ student: 'missingStudent@gmail.com' })

    expect(res.statusCode).to.equal(404)
    expect(res.body.message).to.have.equal('Student not found')
  })

  afterEach(async () => {
    await mysqlHelper.afterEach()
  })

  after(async () => {
    await mysqlHelper.after()
  })
})
