import app from '../../src/init/express'
import chai from 'chai'
import request from 'supertest'
import mysqlHelper from '../helpers/mysql.helper'
import _ from 'lodash'

const expect = chai.expect

const dataSets = [
  {
    teacher: 'teacher1@gmail.com',
    students: [
      'student1@gmail.com',
      'student2@gmail.com',
      'teacher1student@gmail.com'
    ]
  },
  {
    teacher: 'teacher2@gmail.com',
    students: [
      'student1@gmail.com',
      'student2@gmail.com',
      'teacher2studet@gmail.com'
    ]
  }
]

let teachers

describe('#GET /api/commonstudents', () => {
  const url = '/api/commonstudents'

  before(async () => {
    await mysqlHelper.before()
  })

  beforeEach(async () => {
    await mysqlHelper.beforeEach()
    const seed = await mysqlHelper.seeding(dataSets)
    teachers = seed.teachers
  })

  it('should return a empty list if no student under teacher', async () => {
    const res = await request(app)
      .get(url)
      .query({ teacher: 'aloneteacher@gmail.com' })

    const { statusCode, body } = res
    expect(statusCode).to.equal(200)
    expect(body).to.have.own.property('students')
    expect(body.students).to.have.lengthOf(0)
  })

  it('should return a list of students registered under particular teacher', async () => {
    const teacherEmail = teachers[0].email
    const res = await request(app)
      .get(url)
      .query({ teacher: teacherEmail })

    const { statusCode, body } = res
    expect(statusCode).to.equal(200)
    expect(body).to.have.own.property('students')
    expect(body.students).to.have.lengthOf(3)
    expect(body.students).to.deep.equal(
      (dataSets.find(ds => ds.teacher === teacherEmail)).students
    )
  })

  it('should return a list of common students registered under both teacher', async () => {
    const res = await request(app)
      .get(url)
      .query({ teacher: _.map(dataSets, 'teacher') })

    const { statusCode, body } = res
    expect(statusCode).to.equal(200)
    expect(body).to.have.own.property('students')
    expect(body.students).to.have.lengthOf(2)
    expect(body.students).to.deep.equal(_.intersectionBy(
      ...(_.map(dataSets, 'students'))
    ))
  })

  afterEach(async () => {
    await mysqlHelper.afterEach()
  })

  after(async () => {
    await mysqlHelper.after()
  })
})
