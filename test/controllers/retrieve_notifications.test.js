import app from '../../src/init/express.js'
import chai from 'chai'
import request from 'supertest'
import mysqlHelper from '../helpers/mysql.helper'
import db from '../../src/init/sequelize'

const expect = chai.expect

const dataSets = [
  {
    teacher: 'teacher1@gmail.com',
    students: [
      'student1@gmail.com',
      'student2@gmail.com',
      'teacher1student@gmail.com'
    ]
  },
  {
    teacher: 'teacher2@gmail.com',
    students: [
      'student1@gmail.com',
      'student2@gmail.com',
      'teacher2studet@gmail.com'
    ]
  },
  {
    teacher: 'aloneTeacher@gmail.com'
  }
]

let teachers, students
let notification = `hello students! @mentionedstudent1@gmail.com @mentionedstudent2@gmail.com`
let studentsMentioned = ['mentionedstudent1@gmail.com', 'mentionedstudent2@gmail.com']

describe('#POST /api/retrievefornotifications', () => {
  const url = '/api/retrievefornotifications'

  before(async () => {
    await mysqlHelper.before()
  })

  beforeEach(async () => {
    await mysqlHelper.beforeEach()
    const seed = await mysqlHelper.seeding(dataSets)
    teachers = seed.teachers
    students = seed.students
  })

  describe('without student under teacher', async () => {
    let aloneTeacher = 'aloneTeacher@gmail.com'

    it('should return a empty list if no student under teacher and no mentioned in notification text', async () => {
      const res = await request(app)
        .post(url)
        .send({ teacher: aloneTeacher, notification: 'hello student!' })

      const { statusCode, body: { recipients } } = res
      expect(statusCode).to.equal(200)
      expect(recipients).to.be.an('array').to.have.lengthOf(0)
    })

    it('should return list of recipients (mentioned) in notification text', async () => {
      const res = await request(app)
        .post(url)
        .send({ teacher: aloneTeacher, notification })

      const { statusCode, body: { recipients } } = res
      expect(statusCode).to.equal(200)
      expect(recipients)
        .to.be.an('array')
        .to.have.lengthOf(2)
        .to.be.deep.equal(studentsMentioned)
    })
  })

  describe('with student under teacher', () => {
    it('should return list of recipients (student) under a teacher', async () => {
      const res = await request(app)
        .post(url)
        .send({ teacher: teachers[0].email, notification: 'hello student!' })

      const { statusCode, body: { recipients } } = res
      expect(statusCode).to.equal(200)
      expect(recipients)
        .to.be.an('array')
        .to.have.lengthOf(3)
        .to.be.deep.equal(dataSets[0].students)
    })

    it('should return list of recipients (both) under a teacher and mentioned in notification text', async () => {
      const res = await request(app)
        .post(url)
        .send({ teacher: teachers[0].email, notification })

      const { statusCode, body: { recipients } } = res
      expect(statusCode).to.equal(200)
      expect(recipients.sort())
        .to.be.an('array')
        .to.have.lengthOf(5)
        .to.be.deep.equal([ ...dataSets[0].students, ...studentsMentioned ].sort())
    })

    it('should return a unique list of recipients (both) under a teacher and mentioned in notification text', async () => {
      const res = await request(app)
        .post(url)
        .send({
          teacher: teachers[0].email,
          notification: `hello students! @mentionedstudent1@gmail.com @mentionedstudent2@gmail.com ${dataSets[0].students[0]}`
        })

      const { statusCode, body: { recipients } } = res
      expect(statusCode).to.equal(200)
      expect(recipients.sort())
        .to.be.an('array')
        .to.have.lengthOf(5)
        .to.be.deep.equal([ ...dataSets[0].students, ...studentsMentioned ].sort())
    })
  })

  describe('with suspended student under teacher', () => {
    it('should not include suspended student', async () => {
      // set suspend student
      let suspendStudent = students.find(st => st.email === dataSets[0].students[0])

      // set student suspend to true
      await db.lesson.update({
        suspend: true
      }, {
        where: { studentId: suspendStudent.id }
      })

      // call API
      const res = await request(app)
        .post(url)
        .send({ teacher: teachers[0].email, notification })

      const { statusCode, body: { recipients } } = res
      expect(statusCode).to.equal(200)
      expect(recipients)
        .to.be.an('array')
        .to.have.lengthOf(4)
        .to.not.include(suspendStudent.email)
    })
  })

  afterEach(async () => {
    await mysqlHelper.afterEach()
  })

  after(async () => {
    await mysqlHelper.after()
  })
})
