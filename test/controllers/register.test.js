import app from '../../src/init/express.js'
import chai from 'chai'
import request from 'supertest'
import mysqlHelper from '../helpers/mysql.helper'
import db from '../../src/init/sequelize'

const expect = chai.expect

describe('#POST /api/register', () => {
  const url = '/api/register'
  const body = {
    teacher: 'teacher1@gmail.com',
    students: [
      'student1@gmail.com',
      'student2@gmail.com'
    ]
  }

  before(async () => {
    await mysqlHelper.before()
  })

  beforeEach(async () => {
    await mysqlHelper.beforeEach()
  })

  it('should successful register student', async () => {
    const res = await request(app)
      .post(url)
      .send(body)

    const lessons = await db.lesson.findAll()

    expect(res.statusCode).to.equal(204)
    expect(Object.keys(res.body)).to.have.lengthOf(0)
    expect(lessons).to.have.length(2)
  })

  it('should fail if teacher email not valid', async () => {
    const res = await request(app)
      .post(url)
      .send({ ...body, teacher: 'teacher' })

    const { statusCode, body: { message } } = res
    expect(statusCode).to.equal(400)
    expect(message).to.equal('"teacher" must be a valid email')
  })

  it('should fail if student email not valid', async () => {
    const res = await request(app)
      .post(url)
      .send({ ...body, students: ['something'] })

    const { statusCode, body: { message } } = res
    expect(statusCode).to.equal(400)
    expect(message).to.contain('must be a valid email')
  })

  it('should not duplicate lessons', async () => {
    await request(app)
      .post(url)
      .send(body)

    const res = await request(app)
      .post(url)
      .send(body)

    const lessons = await db.lesson.findAll()

    expect(res.statusCode).to.equal(204)
    expect(Object.keys(res.body)).to.have.lengthOf(0)
    expect(lessons).to.have.length(2)
  })

  afterEach(async () => {
    await mysqlHelper.afterEach()
  })

  after(async () => {
    await mysqlHelper.after()
  })
})
