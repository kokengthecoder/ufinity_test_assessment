import mysqlHelper from '../helpers/mysql.helper'

describe('Test Controllers', () => {
  before(async () => {
    await mysqlHelper.before()
  })

  require('./register.test')
  require('./common_students.test.js')
  require('./suspend.test')
  require('./retrieve_notifications.test')

  after(async () => {
    await mysqlHelper.after()
  })
})
