import mysqlHelper from '../helpers/mysql.helper'

describe('Test Service', () => {
  before(async () => {
    await mysqlHelper.before()
  })

  require('./teacher.test')

  after(async () => {
    await mysqlHelper.after()
  })
})
