import chai from 'chai'
import db from '../../src/init/sequelize'
import TeacherService from '../../src/services/teacher.service'
import mysqlHelper from '../helpers/mysql.helper'

const expect = chai.expect

const teacherEmail = 'teacher1@gmail.com'
const studentEmails = [
  'student1@gmail.com',
  'student2@gmail.com'
]

describe('Services: Teacher', () => {
  // re-create tables before each test
  beforeEach(async () => {
    await mysqlHelper.beforeEach()
  })

  it('should register teacher, student and lessons', async () => {
    const lessons = await TeacherService
      .registerStudentAndLesson(teacherEmail, studentEmails)

    expect(lessons).to.have.length(2)
  })

  it('should return error if email not valid', async () => {
    try {
      await TeacherService
        .registerStudentAndLesson('something', studentEmails)
    } catch (err) {
      expect(err.message).to.be.equal('Validation error: Validation isEmail on email failed')
    }
  })

  it('should not duplicate teacher', async () => {
    await TeacherService
      .registerStudentAndLesson(teacherEmail, studentEmails)

    const lessons = await TeacherService
      .registerStudentAndLesson(teacherEmail, ['student@gmail.com'])

    expect(lessons[0].teacherId).to.equal(1)
  })

  it('should not duplicate lessons', async () => {
    await TeacherService
      .registerStudentAndLesson(teacherEmail, [studentEmails[0]])

    const lessons = await TeacherService
      .registerStudentAndLesson(teacherEmail, [studentEmails[0]])

    expect(lessons).to.be.length(0)
  })

  it('should create lessons for who has not registered', async () => {
    await TeacherService
      .registerStudentAndLesson(teacherEmail, [studentEmails[0]])

    const lessons = await TeacherService
      .registerStudentAndLesson(teacherEmail, studentEmails)

    expect(lessons).to.be.length(1)
  })

  it('should create lessons if student is already registered in student table', async () => {
    await db.student.create({ email: studentEmails[0] })

    const lessons = await TeacherService
      .registerStudentAndLesson(teacherEmail, [studentEmails[0]])

    expect(lessons).to.be.length(1)
    expect(lessons[0].studentId).to.equal(1)
  })

  it('should return error if student email not valid', async () => {
    try {
      await TeacherService.registerStudentAndLesson(teacherEmail, ['something'])
    } catch (err) {
      expect(err.message).to.equal('aggregate error')
      expect(err[0].message).to.equal('Validation error: Validation isEmail on email failed')
    }
  })

  after(async () => {
    // drop table after all tests completed
    await mysqlHelper.afterEach()
  })
})
